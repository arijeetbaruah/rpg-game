// Fill out your copyright notice in the Description page of Project Settings.


#include "RPGAssetManager.h"
#include "AbilitySystemGlobals.h"

void URPGAssetManager::StartInitialLoading()
{
	Super::StartInitialLoading();

	UAbilitySystemGlobals::Get().InitGlobalData();
	UE_LOG(LogTemp, Warning, TEXT("URPGAssetManager::StartInitialLoading()"));
}
