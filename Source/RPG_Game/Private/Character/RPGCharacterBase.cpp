// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/RPGCharacterBase.h"
#include "AbilitySystemComponent.h"
#include "Attributes/CharacterAttributeSetBase.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Character/Abilities/CharacterAbilitySystemComponent.h"
#include "Character/Abilities/RPGCharacterGameplayAbility.h"

// Sets default values
ARPGCharacterBase::ARPGCharacterBase(const FObjectInitializer& ObjectInitializer)
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bAbilitySystemComponent = CreateDefaultSubobject<UCharacterAbilitySystemComponent>(TEXT("AbilitySystemComponent"));

	GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Block);

	bAlwaysRelevant = true;

	//DeadTag = FGameplayTag::RequestGameplayTag(FName("State.Dead"));
	//EffectRemoveOnDeathTag = FGameplayTag::RequestGameplayTag(FName("Effect.RemoveOnDeath"));
}

bool ARPGCharacterBase::IsAlive() const
{
	return GetHealth() > 0.0f;
}

int32 ARPGCharacterBase::GetAbilityLevel(RPG_GameAbilityID AbilityID) const
{
	return 1;
}

void ARPGCharacterBase::RemoveCharacterAbilites()
{
	if (GetLocalRole() != ROLE_Authority || !IsValid(GetAbilitySystemComponent()) || !bAbilitySystemComponent->CharacterAbilitiesGiven)
	{
		return;
	}

	for(const FGameplayAbilitySpec& Spec : GetAbilitySystemComponent()->GetActivatableAbilities())
	{
		if (Spec.SourceObject == this && CharacterAbilities.Contains(Spec.Ability->GetClass()))
		{
			bAbilitySystemComponent->ClearAbility(Spec.Handle);
		}
	}

	bAbilitySystemComponent->CharacterAbilitiesGiven = false;
}

void ARPGCharacterBase::Die()
{
	if (!IsAlive())
	{
		return;
	}

	RemoveCharacterAbilites();
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetCharacterMovement()->GravityScale = 0.0f;
	GetCharacterMovement()->Velocity = FVector(0);

	OnCharacterDead.Broadcast(this);

	if (bAbilitySystemComponent.IsValid())
	{
		bAbilitySystemComponent->CancelAbilities();

		FGameplayTagContainer EffectTagsToRemove;
		EffectTagsToRemove.AddTag(EffectRemoveOnDeathTag);
		int32 NumEffectsRemoved = bAbilitySystemComponent->RemoveActiveEffectsWithTags(EffectTagsToRemove);
		bAbilitySystemComponent->AddLooseGameplayTag(DeadTag);
	}

	if (DeathMontage)
	{
		PlayAnimMontage(DeathMontage);
	}
	else {
		FinishDying();
	}
}

void ARPGCharacterBase::FinishDying()
{
	Destroy();
}

float ARPGCharacterBase::GetHealth() const
{
	if (AttributeSetBase.IsValid())
	{
		return AttributeSetBase->GetHealth();
	}

	return 0.0f;
}

float ARPGCharacterBase::GetMaxHealth() const
{
	if (AttributeSetBase.IsValid())
	{
		return AttributeSetBase->GetMaxHealth();
	}

	return 0.0f;
}

float ARPGCharacterBase::GetStamina() const
{
	if (AttributeSetBase.IsValid())
	{
		return AttributeSetBase->GetStamina();
	}

	return 0.0f;
}

float ARPGCharacterBase::GetMaxStamina() const
{
	if (AttributeSetBase.IsValid())
	{
		return AttributeSetBase->GetMaxStamina();
	}

	return 0.0f;
}

// Called when the game starts or when spawned
void ARPGCharacterBase::BeginPlay()
{
	Super::BeginPlay();
	
}

void ARPGCharacterBase::InitializeAttributes()
{
}

void ARPGCharacterBase::AddCharacterAbilities()
{
	if (GetLocalRole() != ROLE_Authority || !IsValid(GetAbilitySystemComponent()) || bAbilitySystemComponent->CharacterAbilitiesGiven)
	{
		return;
	}

	for(TSubclassOf<URPGCharacterGameplayAbility>& Ability : CharacterAbilities)
	{
		//bAbilitySystemComponent->GiveAbility(FGameplayAbilitySpec(Ability, GetAbilityLevel(Ability.GetDefaultObject()->AbilityID), INDEX_NONE, this));
	}
}

void ARPGCharacterBase::AddStartupEffects()
{
}

void ARPGCharacterBase::SetHealth(float Health)
{
}

void ARPGCharacterBase::SetStamina(float Stamina)
{
}

// Called every frame
void ARPGCharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ARPGCharacterBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

UAbilitySystemComponent* ARPGCharacterBase::GetAbilitySystemComponent() const
{
	UAbilitySystemComponent* asc = Cast<UAbilitySystemComponent>(bAbilitySystemComponent.Get());
	return asc;
}

