// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"
#include "RPGCharacterGameplayAbility.generated.h"

/**
 * 
 */
UCLASS()
class RPG_GAME_API URPGCharacterGameplayAbility : public UGameplayAbility
{
	GENERATED_BODY()
	
};
