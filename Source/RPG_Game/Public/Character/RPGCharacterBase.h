// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "AbilitySystemInterface.h"
#include "GameplayTagContainer.h"
#include <RPG_Game/RPG_Game.h>
#include "RPGCharacterBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCharacterDeadDelegate, ARPGCharacterBase*, Character);

class UAbilitySystemComponent;
class UCharacterAbilitySystemComponent;
class UCharacterAttributeSetBase;
class URPGCharacterGameplayAbility;

UCLASS()
class RPG_GAME_API ARPGCharacterBase : public ACharacter, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ARPGCharacterBase(const class FObjectInitializer& ObjectInitializer);

	UPROPERTY(BlueprintAssignable, Category = "RPG|Character")
	FCharacterDeadDelegate OnCharacterDead;

public:

	UFUNCTION(BlueprintCallable, Category = "RPG|Character")
	virtual bool IsAlive() const;

	UFUNCTION(BlueprintCallable, Category = "RPG|Character")
	virtual int32 GetAbilityLevel(RPG_GameAbilityID AbilityID) const;

	virtual void RemoveCharacterAbilites();

	virtual void Die();

	UFUNCTION(BlueprintCallable, Category = "RPG|Character")
	virtual void FinishDying();

	UFUNCTION(BlueprintCallable, Category = "RPG|Character|Attribute")
	virtual float GetHealth() const;

	UFUNCTION(BlueprintCallable, Category = "RPG|Character|Attribute")
	virtual float GetMaxHealth() const;

	UFUNCTION(BlueprintCallable, Category = "RPG|Character|Attribute")
	virtual float GetStamina() const;

	UFUNCTION(BlueprintCallable, Category = "RPG|Character|Attribute")
	virtual float GetMaxStamina() const;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	FGameplayTag DeadTag;
	FGameplayTag EffectRemoveOnDeathTag;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "RPG|Character")
	FText CharacterName;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "RPG|Animation")
	UAnimMontage* DeathMontage;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "RPG|Abilities")
	TSubclassOf<class URPGCharacterGameplayAbility> DefaultAttributes;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "RPG|Abilities")
	TArray<TSubclassOf<class URPGCharacterGameplayAbility>> CharacterAbilities;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "RPG|Abilities")
	TArray<TSubclassOf<class UGameplayEffect>> StartupEffects;

	virtual void InitializeAttributes();
	virtual void AddCharacterAbilities();
	virtual void AddStartupEffects();

	virtual void SetHealth(float Health);
	virtual void SetStamina(float Stamina);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	TWeakObjectPtr<UCharacterAttributeSetBase> AttributeSetBase;
	TWeakObjectPtr<UCharacterAbilitySystemComponent> bAbilitySystemComponent;

	// Inherited via IAbilitySystemInterface
	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override;

};
